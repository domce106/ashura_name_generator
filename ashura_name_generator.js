const male_3_syl = .2;
const female_3_syl = .33;
const female_4_syl = .05;

const male_first_syl = ['A', 'Ash', 'An', 'Bav', 'Beh', 'Dav', 'Da', 'El', 'En', 'Fa', 'Ga',
            'Ge', 'Hor', 'Il', 'I', 'Kah', 'Kol', 'Lem', 'La', 'Las', 'Me', 'Ma',
            'Mo', 'Ne', 'Nos', 'Nar', 'Ok', 'O', 'Pe', 'Pa', 'Pen', 'Pent', 'Ro', 'Ra',
            'Sen', 'Sep', 'Sa', 'Se', 'Sha', 'Mol', 'Be', 'She', 'Ta', 'Ta', 'Tyl', 'Ul',
            'Un', 'Ves', 'Va', 'Yan', 'Ya', 'Zan', 'Zi', 'Zem', 'Zi', 'Zha', 'Zhe'];

const male_mid_syl = ['ka', 'ha', 'mar', 'za', 'tu', 'ma', 'mu', 'run', 'li', 'y', 'nee'];

const male_last_syl = ['lar', 'har', 'dras', 'nor', 'heer', 'ravd', 'gor', 'du', 'raar', 'zid', 'laat',
                         'dun', 'gur', 'lod', 'du', 'ner', 'hood', 'red', 'dor', 'vaar', 'run', 'had',
                         'gos', 'deer', 'lan', 'ter', 'van', 'kos', 'tos', 'daar', 'ritt', 'mnu', 'dros',
                         'war', 'kor', 'raad'];


const female_first_syl = ['An', 'Ash', 'Be', 'Bar', 'E', 'Esh', 'Fa', 'Fa', 'Fe', 'Gi', 'Ga',
                            'Hir', 'He', 'I', 'In', 'Il', 'Kai', 'Ka', 'Keh', 'Ko', 'Ki',
                            'Ke', 'Lee', 'La', 'Me', 'Ma', 'Nau', 'Nah', 'O', 'Ok', 'Par',
                            'Pa', 'Rash', 'Raw', 'Ri', 'Rah', 'Sih', 'Si', 'She', 'Sa', 'Ta',
                            'Te', 'Ul', 'Ve', 'Va', 'Vash', 'Yan', 'Za', 'Ze'];

const female_mid_syl = ['ka', 'ha', 'mar', 'za', 'tu', 'ma', 'mu', 'run', 'li', 'y', 'nee',
                          'iv', 've', 're', 'mi', 'shan', 'ni', 'va', 'la', 'i', 'lee', 'an'];

const female_last_syl = ['da', 'dra', 'ra', 'ti', 'na', 'ah', 'nu', 'rim', 'la', 'leah', 'dah', 'rah', 'dra',
                           'nith', 'va', 'ya', 'niah', 'via', 'ga', 'ri', 'ta', 'ria', 'tha', 'na', 'na', 'na'
                           , 'na', 'na', 'na', 'ra', 'ra', 'ra', 'ra', 'im'];

generatedNames = [];

function random_select(values)
{
  return (values[Math.floor(Math.random() * values.length)]);
}

function male_names()
{
  if (male_3_syl > Math.random())
  {
    print = random_select(male_first_syl) + random_select(male_mid_syl) + random_select(male_last_syl);
  }
  else
  {
    print = random_select(male_first_syl) + random_select(male_last_syl);
  }

  return (print)
}

function female_names()
{
  check = Math.random()

  if (female_3_syl > check)
  {
    print = random_select(female_first_syl) + random_select(female_mid_syl) + random_select(female_last_syl);
  }
  else if (female_4_syl > check)
  {
    print = random_select(female_first_syl) + random_select(female_mid_syl) + random_select(female_mid_syl) + random_select(female_last_syl);
  }
  else
  {
    print = random_select(female_first_syl) + random_select(female_last_syl);
  }

  return (print)
}


function name_printer()
{
  let numberOfNames = document.getElementById("number").valueAsNumber;
  let gender = document.querySelector('input[name="genderPick"]:checked').value;
  let print_names = [];
  let nameOfGender ;


  for (let i = 0; i < numberOfNames; i++)
  {
    let holder = gender == "female" ? female_names() : male_names();
    if (generatedNames.includes(holder))
    {
      i--;
    }
    else
    {
      print_names.push(holder);
      generatedNames.push(holder);
      nameOfGender = gender == "female" ? "Female" : "Male";
    }
    //print_names.push(gender == "female" ? female_names() : male_names());
  }


  print_names = print_names.join(',  ')

  document.getElementById("genderType").innerHTML = nameOfGender + " name(s):";
  document.getElementById("printNames").innerHTML = print_names;

}
